//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MovieReservation.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Ticket
    {
        public int Ticket_Id { get; set; }
        public System.DateTime DateTime { get; set; }
        public int Status { get; set; }
        public int TimeScheduleSchedule_Id { get; set; }
        public int SeatSeat_Id { get; set; }
        public int UserId { get; set; }
    
        public virtual TimeSchedule TimeSchedule { get; set; }
        public virtual Seat Seat { get; set; }
        public virtual User User { get; set; }
    }
}
