
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 11/21/2017 22:23:08
-- Generated from EDMX file: C:\Users\illumillal\Documents\datacentric\MovieReservation\MovieReservation\Models\ReservationModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ReservationDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_CinemaTimeSchedule_Cinema]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CinemaTimeSchedule] DROP CONSTRAINT [FK_CinemaTimeSchedule_Cinema];
GO
IF OBJECT_ID(N'[dbo].[FK_CinemaTimeSchedule_TimeSchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CinemaTimeSchedule] DROP CONSTRAINT [FK_CinemaTimeSchedule_TimeSchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_MovieTimeSchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TimeSchedules] DROP CONSTRAINT [FK_MovieTimeSchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_SeatType_Seat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Seats] DROP CONSTRAINT [FK_SeatType_Seat];
GO
IF OBJECT_ID(N'[dbo].[FK_RoomSeat]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Seats] DROP CONSTRAINT [FK_RoomSeat];
GO
IF OBJECT_ID(N'[dbo].[FK_ReservationTimeSchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_ReservationTimeSchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_RoomTimeSchedule]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TimeSchedules] DROP CONSTRAINT [FK_RoomTimeSchedule];
GO
IF OBJECT_ID(N'[dbo].[FK_SeatTicket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_SeatTicket];
GO
IF OBJECT_ID(N'[dbo].[FK_UserTicket]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Tickets] DROP CONSTRAINT [FK_UserTicket];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Movies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Movies];
GO
IF OBJECT_ID(N'[dbo].[Cinemas]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cinemas];
GO
IF OBJECT_ID(N'[dbo].[TimeSchedules]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TimeSchedules];
GO
IF OBJECT_ID(N'[dbo].[Rooms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rooms];
GO
IF OBJECT_ID(N'[dbo].[Tickets]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Tickets];
GO
IF OBJECT_ID(N'[dbo].[Seats]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Seats];
GO
IF OBJECT_ID(N'[dbo].[Type_Seat]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Type_Seat];
GO
IF OBJECT_ID(N'[dbo].[CinemaTimeSchedule]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CinemaTimeSchedule];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Gender] nvarchar(max)  NOT NULL,
    [DOB] nvarchar(max)  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [Picture] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Movies'
CREATE TABLE [dbo].[Movies] (
    [Movie_Id] int IDENTITY(1,1) NOT NULL,
    [Movie_Name] nvarchar(max)  NOT NULL,
    [Time] int  NOT NULL,
    [Director] nvarchar(max)  NOT NULL,
    [Synopsis] nvarchar(max)  NOT NULL,
    [Rate] nvarchar(max)  NOT NULL,
    [Genre] nvarchar(max)  NOT NULL,
    [Release_date] datetime  NOT NULL,
    [Movie_Photo] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Cinemas'
CREATE TABLE [dbo].[Cinemas] (
    [Cinema_Id] int IDENTITY(1,1) NOT NULL,
    [Cinema_name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TimeSchedules'
CREATE TABLE [dbo].[TimeSchedules] (
    [Schedule_Id] int IDENTITY(1,1) NOT NULL,
    [Time] datetime  NOT NULL,
    [Language] nvarchar(max)  NOT NULL,
    [Movie_Id] int  NOT NULL,
    [Room_Id] int  NOT NULL
);
GO

-- Creating table 'Rooms'
CREATE TABLE [dbo].[Rooms] (
    [Room_Id] int IDENTITY(1,1) NOT NULL,
    [Type] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Tickets'
CREATE TABLE [dbo].[Tickets] (
    [Ticket_Id] int IDENTITY(1,1) NOT NULL,
    [DateTime] datetime  NOT NULL,
    [Status] int  NOT NULL,
    [TimeScheduleSchedule_Id] int  NOT NULL,
    [SeatSeat_Id] int  NOT NULL,
    [UserId] int  NOT NULL
);
GO

-- Creating table 'Seats'
CREATE TABLE [dbo].[Seats] (
    [Seat_Id] int IDENTITY(1,1) NOT NULL,
    [Position] nvarchar(max)  NOT NULL,
    [Type_SeatTypeSeat_Id] int  NOT NULL,
    [Room_Id] int  NOT NULL
);
GO

-- Creating table 'Type_Seat'
CREATE TABLE [dbo].[Type_Seat] (
    [TypeSeat_Id] int IDENTITY(1,1) NOT NULL,
    [TypeSeat] nvarchar(max)  NOT NULL,
    [Cost] int  NOT NULL
);
GO

-- Creating table 'CinemaTimeSchedule'
CREATE TABLE [dbo].[CinemaTimeSchedule] (
    [Cinemas_Cinema_Id] int  NOT NULL,
    [TimeSchedules_Schedule_Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Movie_Id] in table 'Movies'
ALTER TABLE [dbo].[Movies]
ADD CONSTRAINT [PK_Movies]
    PRIMARY KEY CLUSTERED ([Movie_Id] ASC);
GO

-- Creating primary key on [Cinema_Id] in table 'Cinemas'
ALTER TABLE [dbo].[Cinemas]
ADD CONSTRAINT [PK_Cinemas]
    PRIMARY KEY CLUSTERED ([Cinema_Id] ASC);
GO

-- Creating primary key on [Schedule_Id] in table 'TimeSchedules'
ALTER TABLE [dbo].[TimeSchedules]
ADD CONSTRAINT [PK_TimeSchedules]
    PRIMARY KEY CLUSTERED ([Schedule_Id] ASC);
GO

-- Creating primary key on [Room_Id] in table 'Rooms'
ALTER TABLE [dbo].[Rooms]
ADD CONSTRAINT [PK_Rooms]
    PRIMARY KEY CLUSTERED ([Room_Id] ASC);
GO

-- Creating primary key on [Ticket_Id] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [PK_Tickets]
    PRIMARY KEY CLUSTERED ([Ticket_Id] ASC);
GO

-- Creating primary key on [Seat_Id] in table 'Seats'
ALTER TABLE [dbo].[Seats]
ADD CONSTRAINT [PK_Seats]
    PRIMARY KEY CLUSTERED ([Seat_Id] ASC);
GO

-- Creating primary key on [TypeSeat_Id] in table 'Type_Seat'
ALTER TABLE [dbo].[Type_Seat]
ADD CONSTRAINT [PK_Type_Seat]
    PRIMARY KEY CLUSTERED ([TypeSeat_Id] ASC);
GO

-- Creating primary key on [Cinemas_Cinema_Id], [TimeSchedules_Schedule_Id] in table 'CinemaTimeSchedule'
ALTER TABLE [dbo].[CinemaTimeSchedule]
ADD CONSTRAINT [PK_CinemaTimeSchedule]
    PRIMARY KEY CLUSTERED ([Cinemas_Cinema_Id], [TimeSchedules_Schedule_Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Cinemas_Cinema_Id] in table 'CinemaTimeSchedule'
ALTER TABLE [dbo].[CinemaTimeSchedule]
ADD CONSTRAINT [FK_CinemaTimeSchedule_Cinema]
    FOREIGN KEY ([Cinemas_Cinema_Id])
    REFERENCES [dbo].[Cinemas]
        ([Cinema_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [TimeSchedules_Schedule_Id] in table 'CinemaTimeSchedule'
ALTER TABLE [dbo].[CinemaTimeSchedule]
ADD CONSTRAINT [FK_CinemaTimeSchedule_TimeSchedule]
    FOREIGN KEY ([TimeSchedules_Schedule_Id])
    REFERENCES [dbo].[TimeSchedules]
        ([Schedule_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CinemaTimeSchedule_TimeSchedule'
CREATE INDEX [IX_FK_CinemaTimeSchedule_TimeSchedule]
ON [dbo].[CinemaTimeSchedule]
    ([TimeSchedules_Schedule_Id]);
GO

-- Creating foreign key on [Movie_Id] in table 'TimeSchedules'
ALTER TABLE [dbo].[TimeSchedules]
ADD CONSTRAINT [FK_MovieTimeSchedule]
    FOREIGN KEY ([Movie_Id])
    REFERENCES [dbo].[Movies]
        ([Movie_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_MovieTimeSchedule'
CREATE INDEX [IX_FK_MovieTimeSchedule]
ON [dbo].[TimeSchedules]
    ([Movie_Id]);
GO

-- Creating foreign key on [Type_SeatTypeSeat_Id] in table 'Seats'
ALTER TABLE [dbo].[Seats]
ADD CONSTRAINT [FK_SeatType_Seat]
    FOREIGN KEY ([Type_SeatTypeSeat_Id])
    REFERENCES [dbo].[Type_Seat]
        ([TypeSeat_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SeatType_Seat'
CREATE INDEX [IX_FK_SeatType_Seat]
ON [dbo].[Seats]
    ([Type_SeatTypeSeat_Id]);
GO

-- Creating foreign key on [Room_Id] in table 'Seats'
ALTER TABLE [dbo].[Seats]
ADD CONSTRAINT [FK_RoomSeat]
    FOREIGN KEY ([Room_Id])
    REFERENCES [dbo].[Rooms]
        ([Room_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RoomSeat'
CREATE INDEX [IX_FK_RoomSeat]
ON [dbo].[Seats]
    ([Room_Id]);
GO

-- Creating foreign key on [TimeScheduleSchedule_Id] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_ReservationTimeSchedule]
    FOREIGN KEY ([TimeScheduleSchedule_Id])
    REFERENCES [dbo].[TimeSchedules]
        ([Schedule_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ReservationTimeSchedule'
CREATE INDEX [IX_FK_ReservationTimeSchedule]
ON [dbo].[Tickets]
    ([TimeScheduleSchedule_Id]);
GO

-- Creating foreign key on [Room_Id] in table 'TimeSchedules'
ALTER TABLE [dbo].[TimeSchedules]
ADD CONSTRAINT [FK_RoomTimeSchedule]
    FOREIGN KEY ([Room_Id])
    REFERENCES [dbo].[Rooms]
        ([Room_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RoomTimeSchedule'
CREATE INDEX [IX_FK_RoomTimeSchedule]
ON [dbo].[TimeSchedules]
    ([Room_Id]);
GO

-- Creating foreign key on [SeatSeat_Id] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_SeatTicket]
    FOREIGN KEY ([SeatSeat_Id])
    REFERENCES [dbo].[Seats]
        ([Seat_Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SeatTicket'
CREATE INDEX [IX_FK_SeatTicket]
ON [dbo].[Tickets]
    ([SeatSeat_Id]);
GO

-- Creating foreign key on [UserId] in table 'Tickets'
ALTER TABLE [dbo].[Tickets]
ADD CONSTRAINT [FK_UserTicket]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserTicket'
CREATE INDEX [IX_FK_UserTicket]
ON [dbo].[Tickets]
    ([UserId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------