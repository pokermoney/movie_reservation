﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieReservation.Models;
using System.Net.Http;
using System.Web.WebPages;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace MovieReservation.Controllers
{

    public class SeatsController : Controller
    {
        private ReservationModelContainer db = new ReservationModelContainer();

        // GET: Seats
        public ActionResult Index(int movie_id,int schedule_id)
        {
            var timeSchedule = db.TimeSchedules.Find(schedule_id);
            var seats = db.Seats.Where(s => s.Room_Id == timeSchedule.Room_Id);
            ViewBag.Movie = db.Movies.Find(movie_id);
            ViewBag.TimeSchedule = db.TimeSchedules.Find(schedule_id);
            //var seats = db.Seats.Include(s => s.Type_Seat).Include(s => s.Room);
            return View(seats.ToList());
        }

        // GET: Seats/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seat seat = db.Seats.Find(id);
            if (seat == null)
            {
                return HttpNotFound();
            }
            return View(seat);
        }

        // GET: Seats/Create
        public ActionResult Create()
        {
            ViewBag.Type_SeatTypeSeat_Id = new SelectList(db.Type_Seat, "TypeSeat_Id", "TypeSeat");
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type");
            return View();
        }

        // POST: Seats/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Seat_Id,Position,Type_SeatTypeSeat_Id,Room_Id")] Seat seat)
        {
            if (ModelState.IsValid)
            {
                db.Seats.Add(seat);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Type_SeatTypeSeat_Id = new SelectList(db.Type_Seat, "TypeSeat_Id", "TypeSeat", seat.Type_SeatTypeSeat_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", seat.Room_Id);
            return View(seat);
        }

        // GET: Seats/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seat seat = db.Seats.Find(id);
            if (seat == null)
            {
                return HttpNotFound();
            }
            ViewBag.Type_SeatTypeSeat_Id = new SelectList(db.Type_Seat, "TypeSeat_Id", "TypeSeat", seat.Type_SeatTypeSeat_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", seat.Room_Id);
            return View(seat);
        }

        // POST: Seats/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Seat_Id,Position,Type_SeatTypeSeat_Id,Room_Id")] Seat seat)
        {
            if (ModelState.IsValid)
            {
                db.Entry(seat).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Type_SeatTypeSeat_Id = new SelectList(db.Type_Seat, "TypeSeat_Id", "TypeSeat", seat.Type_SeatTypeSeat_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", seat.Room_Id);
            return View(seat);
        }

        // GET: Seats/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Seat seat = db.Seats.Find(id);
            if (seat == null)
            {
                return HttpNotFound();
            }
            return View(seat);
        }

        // POST: Seats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Seat seat = db.Seats.Find(id);
            db.Seats.Remove(seat);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}
