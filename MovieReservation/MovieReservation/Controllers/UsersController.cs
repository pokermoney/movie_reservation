using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieReservation.Models;
using System.IO;


namespace MovieReservation.Controllers
{
    

    public class UsersController : Controller
    {
   

        public void Session_OnStart()
        {
            Session.Timeout = 525600;
        }
        private ReservationModelContainer db = new ReservationModelContainer();
        // GET: Users/Create
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult login_from(MovieReservation.Models.User userModel)
        {
            using (ReservationModelContainer db = new ReservationModelContainer())
            {
                var userDetail = db.Users.Where(x => x.Username == userModel.Username && x.Password == userModel.Password).FirstOrDefault();
                if (userDetail == null)
                {
                    return View("Index", userModel);
                }
                else
                {
                    Session["userID"] = userDetail.Id;
                    Session["username"] = userDetail.Username;
                    return RedirectToAction("Index", "Movies");

                }
            }
        }

        public ActionResult LogOut()
        {
            if (Session["userID"] == null)
            {
                return RedirectToAction("Index", "Users");
            }
            else
            {
                int userId = (int)Session["userID"];
                Session.Abandon();
                return RedirectToAction("Index", "Users");
            }
          
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,Username,Password,Email,Gender,DOB,Picture,PhoneNumber")] User user)
        {
            if (ModelState.IsValid)
            {
                if(user.Picture == null)
                {
                    user.Picture = "1";
                }
                else
                {
                    string _Path = Server.MapPath("~/Images");
                    string _FileName = Path.GetFileName(Request.Files[0].FileName);
                    Request.Files[0].SaveAs(Path.Combine(_Path, _FileName));

                    user.Picture = _FileName;
                }
                
                
                db.Users.Add(user);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Username,Password,Email,Gender,DOB,PhoneNumber,Picture")] User user)
        {
            if (ModelState.IsValid)
            {
                string _Path = Server.MapPath("~/Images");
                string _FileName = Path.GetFileName(Request.Files[0].FileName);
                Request.Files[0].SaveAs(Path.Combine(_Path, _FileName));

                user.Picture = _FileName;

                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return Redirect("../../Movies/Index");
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);
            db.Users.Remove(user);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
