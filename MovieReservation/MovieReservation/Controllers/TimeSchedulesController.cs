﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieReservation.Models;

namespace MovieReservation.Controllers
{
    public class TimeSchedulesController : Controller
    {
        private ReservationModelContainer db = new ReservationModelContainer();

        // GET: TimeSchedules
        public ActionResult Index()
        {
            var timeSchedules = db.TimeSchedules.Include(t => t.Movie).Include(t => t.Room);
            return View(timeSchedules.ToList());
        }

        // GET: TimeSchedules/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSchedule timeSchedule = db.TimeSchedules.Find(id);
            if (timeSchedule == null)
            {
                return HttpNotFound();
            }
            return View(timeSchedule);
        }

        // GET: TimeSchedules/Create
        public ActionResult Create()
        {
            ViewBag.Movie_Id = new SelectList(db.Movies, "Movie_Id", "Movie_Name");
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type");
            return View();
        }

        // POST: TimeSchedules/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Schedule_Id,Time,Language,Movie_Id,Room_Id")] TimeSchedule timeSchedule)
        {
            if (ModelState.IsValid)
            {
                db.TimeSchedules.Add(timeSchedule);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Movie_Id = new SelectList(db.Movies, "Movie_Id", "Movie_Name", timeSchedule.Movie_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", timeSchedule.Room_Id);
            return View(timeSchedule);
        }

        // GET: TimeSchedules/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSchedule timeSchedule = db.TimeSchedules.Find(id);
            if (timeSchedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.Movie_Id = new SelectList(db.Movies, "Movie_Id", "Movie_Name", timeSchedule.Movie_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", timeSchedule.Room_Id);
            return View(timeSchedule);
        }

        // POST: TimeSchedules/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Schedule_Id,Time,Language,Movie_Id,Room_Id")] TimeSchedule timeSchedule)
        {
            if (ModelState.IsValid)
            {
                db.Entry(timeSchedule).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Movie_Id = new SelectList(db.Movies, "Movie_Id", "Movie_Name", timeSchedule.Movie_Id);
            ViewBag.Room_Id = new SelectList(db.Rooms, "Room_Id", "Type", timeSchedule.Room_Id);
            return View(timeSchedule);
        }

        // GET: TimeSchedules/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSchedule timeSchedule = db.TimeSchedules.Find(id);
            if (timeSchedule == null)
            {
                return HttpNotFound();
            }
            return View(timeSchedule);
        }

        // POST: TimeSchedules/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TimeSchedule timeSchedule = db.TimeSchedules.Find(id);
            db.TimeSchedules.Remove(timeSchedule);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
