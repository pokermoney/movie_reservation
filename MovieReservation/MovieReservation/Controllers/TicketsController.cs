﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MovieReservation.Models;

namespace MovieReservation.Controllers
{
    public class TicketsController : Controller
    {
        private ReservationModelContainer db = new ReservationModelContainer();

        // GET: Tickets
        public ActionResult Index()
        {
            var tickets = db.Tickets.Include(t => t.TimeSchedule).Include(t => t.Seat).Include(t => t.User);
            return View(tickets.ToList());
        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // GET: Tickets/Create
        public ActionResult Create()
        {
            ViewBag.TimeScheduleSchedule_Id = new SelectList(db.TimeSchedules, "Schedule_Id", "Language");
            ViewBag.SeatSeat_Id = new SelectList(db.Seats, "Seat_Id", "Position");
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Ticket_Id,DateTime,Status,TimeScheduleSchedule_Id,SeatSeat_Id,UserId")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Tickets.Add(ticket);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.TimeScheduleSchedule_Id = new SelectList(db.TimeSchedules, "Schedule_Id", "Language", ticket.TimeScheduleSchedule_Id);
            ViewBag.SeatSeat_Id = new SelectList(db.Seats, "Seat_Id", "Position", ticket.SeatSeat_Id);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", ticket.UserId);
            return View(ticket);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            ViewBag.TimeScheduleSchedule_Id = new SelectList(db.TimeSchedules, "Schedule_Id", "Language", ticket.TimeScheduleSchedule_Id);
            ViewBag.SeatSeat_Id = new SelectList(db.Seats, "Seat_Id", "Position", ticket.SeatSeat_Id);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", ticket.UserId);
            return View(ticket);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Ticket_Id,DateTime,Status,TimeScheduleSchedule_Id,SeatSeat_Id,UserId")] Ticket ticket)
        {
            if (ModelState.IsValid)
            {
                db.Entry(ticket).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TimeScheduleSchedule_Id = new SelectList(db.TimeSchedules, "Schedule_Id", "Language", ticket.TimeScheduleSchedule_Id);
            ViewBag.SeatSeat_Id = new SelectList(db.Seats, "Seat_Id", "Position", ticket.SeatSeat_Id);
            ViewBag.UserId = new SelectList(db.Users, "Id", "Username", ticket.UserId);
            return View(ticket);
        }

        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Ticket ticket = db.Tickets.Find(id);
            if (ticket == null)
            {
                return HttpNotFound();
            }
            return View(ticket);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Ticket ticket = db.Tickets.Find(id);
            db.Tickets.Remove(ticket);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult Confirm(string[] values,int cuser, int time)
        {
            Ticket ticket = new Ticket();
            TimeSchedule timeSchedule = db.TimeSchedules.Find(time);
            User user = db.Users.Find(cuser);
            for (int i = 0; i < values.Length; i++)
            {
                Seat seat = db.Seats.Find(Int32.Parse(values[i]));
                ticket.DateTime = DateTime.Now;
                ticket.Status = 0;
                ticket.TimeScheduleSchedule_Id = time;
                ticket.SeatSeat_Id = Int32.Parse(values[i]);
                ticket.UserId = cuser;
                ticket.TimeSchedule = timeSchedule;
                ticket.Seat = seat;
                ticket.User = user;
                seat.Tickets.Add(ticket);
                timeSchedule.Reservations.Add(ticket);
                user.Tickets.Add(ticket);
                db.Tickets.Add(ticket);
                db.SaveChanges();
            }
            
            return Json(new { message = "ok" });
        }
    }
}
